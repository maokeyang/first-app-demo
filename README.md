
```
// 查看本地分支
git tag

// 删除本地tagname 的tag
git tag -d tagname

// 删除远程tagname 的tag （推一个空的源标签即为删除）
git push origin :refs/tags/tagname

// 指定某次commit id， 打tag为 tagname
git tag tagname 3798dd9161ddc97f75a3eb3220e292b791934c6b

// 推送标签
git push origin v1.0

// 推送本地所有标签
git push origin --tags

```

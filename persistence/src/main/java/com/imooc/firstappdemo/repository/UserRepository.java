package com.imooc.firstappdemo.repository;

import com.imooc.firstappdemo.domain.User;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Repository
public class UserRepository {

    private ConcurrentHashMap<Integer, User> repository = new ConcurrentHashMap<>();

    private final static AtomicInteger idGen = new AtomicInteger(0);

    public boolean save(User user) {
        int i = idGen.incrementAndGet();

        user.setId(i);

        return repository.put(i, user) == null;
    }


//    public List<User> findAll() {
//        List<User> users = new ArrayList<>();
//        System.out.println("当前的ID值是id=" + idGen.get());
//        for (int i = 1; i <= idGen.get(); i++) {
//            System.out.println("i=" + i);
//            users.add(repository.get(i));
//        }
//        return users;
//    }


    public Collection<User> findAll() {
        return repository.values();
    }
}

package com.imooc.firstappdemo.config;

import com.imooc.firstappdemo.domain.User;
import com.imooc.firstappdemo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Flux;

import java.util.Collection;

/**
 * 路由器函数配置
 */
@Configuration
public class RouterFunctionConfiguration {

    /**
     *  Servlet
     *
     * 请求接口: ServletRequest 或者 HttpServletRequest
     *
     * 响应接口: ServletResponse 或者 HttpServletResponse
     *
     * Spring5.0 重新定义了服务端的请求和响应接口
     *
     * 请求接口： ServerRequest
     *          ServerResponse
     *
     * 即可支持Servlet规范  也可以支持自定义 比如Netty
     *
     *
     * Flux是 0-N 个对象集合
     * Mono 是 0-1 个对象集合
     * Reactive中的Flux或者mono 是异步处理（非阻塞）
     *
     * 集合独享基本是同步处理（阻塞）
     *
     * Flux Mono 都是Publisher
     *
     */
    @Bean
    @Autowired
    public RouterFunction<ServerResponse> personFindAll(UserRepository userRepository) {
        Collection<User> users = userRepository.findAll();

        return RouterFunctions.route(RequestPredicates.GET("/person/find/all"),
                request -> {
                    Flux<User> userFlux = Flux.fromIterable(users);
                   return ServerResponse.ok().body(userFlux, User.class);
                });
    }
}

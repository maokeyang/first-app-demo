package com.imooc.firstappdemo.controller;

import com.imooc.firstappdemo.domain.User;
import com.imooc.firstappdemo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    public final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping("/person/save")
    public User save(@RequestParam String name) {

        User user = new User();

        user.setName(name);

        if (userRepository.save(user)) {
            System.out.println("用户对象：" + user + "保存成功!");
        }


        return user;
    }

//    @RequestMapping("/person/find/all")
//    public List<User> getUser() {
//        return userRepository.findAll();
//    }

}
